const https = require('https');
const fs = require('fs');
const express = require('express')
const app = express()
const port = 3000

const options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};


app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-Type,Accept,Authorization')
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')
        return res.status(200).json({})
    }
    next()
})

app.use('/assets', express.static(__dirname + '/assets'))

app.get('/', (req, res) => {
    res.sendFile(__dirname + "/index.html")
})

app.get('/index.js', (req, res) => {
    res.sendFile(__dirname + "/index.js")
})

https.createServer(options, app).listen(port, () => {
    console.log('Listen on port ',port)
})

