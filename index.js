//variables
var vrView, media, count = 0
media = [
    {
        type: 'video',
        height: '100%',
        width: '100%',
        isStereo: true,
        url: "./assets/congo_2048.mp4",
        hotspots: [
            {
                id: 'to-taj-mahal',
                pitch: 30,
                yaw: 20,
                radius: 0.05,
                distance: 2
            }
        ]
    },
    {
        type: 'image',
        height: '100%',
        width: '100%',
        isStereo: false,
        url: "./assets/taj-mahal.jpg",
        hotspots: [
            {
                id: 'to-congo',
                pitch: 20,
                yaw: 40,
                radius: 0.05,
                distance: 1
            }
        ]
    }
]

//events
window.addEventListener('load', onVrViewLoad);

function onVrViewLoad() {
    // Selector '#vrview' finds element with id 'vrview'.
    vrView = new VRView.Player('#vrview', {
        height: media[0].height,
        width: media[0].width,
        ...media[0].type == 'video' ? { video: media[0].url } : { image: media[0].url },
        is_stereo: media[0].isStereo
    });
    vrView.on("play", addHotspots)

    vrView.on('click', function (event) {
        console.log('event', event)
        media[count].hotspots.forEach((element) => {
            if (event.id == element.id) {
                updateContent()
            }
        })

    });
}

function addHotspots() {
    media[count].hotspots.forEach(element => {
        vrView.addHotspot(element.id, {
            pitch: element.pitch, // In degrees. Up is positive.
            yaw: element.yaw, // In degrees. To the right is positive.
            radius: element.radius, // Radius of the circular target in meters.
            distance: element.distance // Distance of target from camera in meters.
        });
    });

}

function updateContent() {
    count == (media.length - 1) ? count = 0 : count++
    vrView.setContent({
        ...media[count].type == 'video' ? { video: media[count].url } : { image: media[count].url },
        is_stereo: media[count].isStereo
    });
    addHotspots()
}